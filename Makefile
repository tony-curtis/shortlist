SOURCE = l.tex
OUTPUT = $(SOURCE:.tex=.pdf)

.PHONY: all tidy clean

all:	$(OUTPUT)

$(OUTPUT):	$(SOURCE)
	latexmk -pdf $^

tidy:
	rm -f *.aux *.fdb* *.fls *.log

clean:	tidy
	rm -f *.pdf
